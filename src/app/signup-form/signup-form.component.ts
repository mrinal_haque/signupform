import { Component, OnInit } from '@angular/core';
import { User } from './../User';


@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css']
})
export class SignupFormComponent implements OnInit {

  private gender:string[];
  private user:User;

  ngOnInit() {
  	this.gender = ["Male", "Female", "Other"];
  	this.user = new User({
  		email: "",
  		password: {
  			pwd: "",
  			confirm_pwd: ""
  		},
  		gender: this.gender[0],
  		terms: false,
  	})
  }

  public onFormSubmit({value, valid} : {value:User, valid:boolean}) {
    this.user = value;
  }
}
